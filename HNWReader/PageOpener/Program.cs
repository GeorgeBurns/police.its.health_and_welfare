﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PageOpener
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            string Filename = "";

            if (args.Length != 1)
            {

                MessageBox.Show("You need to pass in a filename as an argument!");
                return;
            }
            else
                Filename = args[0].ToString();


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new frmMain(Filename));
        }
    }
}

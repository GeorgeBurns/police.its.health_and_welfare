﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Microsoft.Win32;
using System.Diagnostics;
using System.Net;
using System.IO;


namespace PageOpener
{
    public partial class frmMain : Form
    {
        [DllImport("USER32.DLL")]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", EntryPoint = "FindWindowEx", SetLastError = true)]
        private static extern IntPtr FindWindowEx(IntPtr hwndParent, uint hwndChildAfter, string lpszClass, string lpszWindow);

        [DllImport("User32.dll", EntryPoint = "PostMessage")]
        private static extern int PostMessage(IntPtr hWnd, uint Msg, uint wParam, uint lParam);

        string HTMLFileName = "";

        bool ReadyForAttachment = false;

        public frmMain(string FileName)
        {
            this.HTMLFileName = FileName;
            InitializeComponent();
        }

        string URL = "";

        static string BrowserKeyPath = @"\SOFTWARE\Microsoft\Internet Explorer\MAIN\FeatureControl\FEATURE_BROWSER_EMULATION";
        //32bit OS
        //\Software\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION
        //32bit app on 64bit OS
        //\SOFTWARE\Wow6432Node\Microsoft\Internet Explorer\MAIN\FeatureControl\FEATURE_BROWSER_EMULATION
        public static void RemoveBrowserKey()
        {
            RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(BrowserKeyPath.Substring(1), true);
            key.DeleteValue(Process.GetCurrentProcess().ProcessName + ".exe", false);

        }
        public static void CreateBrowserKey(bool IgnoreIDocDirective = false)
        {
            string basekey = Microsoft.Win32.Registry.CurrentUser.ToString();
            Int32 value = 11000;
            //Value reference: http://msdn.microsoft.com/en-us/library/ee330730%28v=VS.85%29.aspx
            //IDOC Reference:  http://msdn.microsoft.com/en-us/library/ms535242%28v=vs.85%29.aspx
            WebBrowser browser = new WebBrowser();

            switch (browser.Version.Major)
            {
                case 8:
                    if (IgnoreIDocDirective)
                    {
                        value = 8888;
                    }
                    else
                    {
                        value = 8000;
                    }
                    break;
                case 9:
                    if (IgnoreIDocDirective)
                    {
                        value = 9999;
                    }
                    else
                    {
                        value = 9000;
                    }
                    break;
                case 10:
                    if (IgnoreIDocDirective)
                    {
                        value = 10001;
                    }
                    else
                    {
                        value = 10000;
                    }
                    break;
                case 11:
                    if (IgnoreIDocDirective)
                    {
                        value = 11001;
                    }
                    else
                    {
                        value = 11000;
                    }
                    break;
                default:
                    break;
            }

            Microsoft.Win32.Registry.SetValue(Microsoft.Win32.Registry.CurrentUser.ToString() + BrowserKeyPath, Process.GetCurrentProcess().ProcessName + ".exe", value, Microsoft.Win32.RegistryValueKind.DWord);
        }

        void client_DownloadDataCompleted(object sender, DownloadDataCompletedEventArgs e)
        {
            //string filepath = textBox1.Text;
            //File.WriteAllBytes(filepath, e.Result);
           // MessageBox.Show("File downloaded");
        }



        private void Form1_Load(object sender, EventArgs e)
        {

            try
            {
                CreateBrowserKey();

                FileInfo fi = new FileInfo(HTMLFileName);

                if (fi.Extension.ToLower() == ".doc")
                {

                    //webBrowser1.Navigate(href);



                    Console.WriteLine("Sleeping for 2 seconds");
                    System.Threading.Thread.Sleep(2000);

                    string FullPath = fi.DirectoryName;

                    System.Diagnostics.Process proc = new System.Diagnostics.Process();
                    proc.EnableRaisingEvents = false;
                    proc.StartInfo.FileName = "HNWDocParser.exe";
                    proc.StartInfo.Arguments =  HTMLFileName;
                    // proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                    proc.StartInfo.UseShellExecute = true;
                    proc.StartInfo.WorkingDirectory = FullPath;

                    proc.Start();
                    proc.WaitForExit();


                    this.Close();

                    try
                    {
                        File.Delete(HTMLFileName);

                    }
                    catch { }

                    try
                    {
                        File.Delete(HTMLFileName.ToLower().Replace(".doc", ".pdf"));

                    }
                    catch { }


                }
                else
                {


                    


                    webBrowser1.Navigate(HTMLFileName);

                }


                // EventHandlers e = new EventHandlers();
                // SHDocVw.InternetExplorer IE = new SHDocVw.InternetExplorer();


                //IE.Navigate(@"C:\securedoc_20150903T131422.html");


            }
            catch (Exception ex)
            {
                MessageBox.Show("Error opening  " + HTMLFileName);
                this.Close();
            
            }

        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {


            //webBrowser1.Focus();
            //SendKeys.SendWait("{TAB}");
            //SendKeys.SendWait("{TAB}");
            //SendKeys.SendWait("{TAB}");
            //SendKeys.SendWait("{TAB}");
            //SendKeys.SendWait("{TAB}");
            //SendKeys.SendWait("{TAB}");
            //SendKeys.SendWait("{TAB}");
            //SendKeys.SendWait("{TAB}");
            //SendKeys.SendWait("{TAB}");



            //return;


            //HtmlElementCollection links1 = webBrowser1.Document.GetElementsByTagName("password");


            //System.Diagnostics.Debug.WriteLine("Hello");


            //foreach (HtmlElement link in links1)
            //{
            ////    System.Diagnostics.Debug.WriteLine(link.InnerText);
            ////    System.Diagnostics.Debug.WriteLine(link.InnerHtml);

            //    //link.InnerText = "1234";

            //    System.Diagnostics.Debug.WriteLine(link.Id);

            //}


         //   return;

            HtmlDocument doc = this.webBrowser1.Document;

            //doc.GetElementById("passwordInput1.text").SetAttribute("Value", "1234");

          //  HtmlElement ee = doc.GetElementById("passwordInput1.text");
           // return;

            if (ReadyForAttachment == true)
            {

                HtmlElementCollection links = webBrowser1.Document.GetElementsByTagName("a");


                foreach (HtmlElement link in links)
                {
                    try
                    {
                        //if (link.InnerText.Equals("My Assigned"))
                        //  link.InvokeMember("Click");

                        // Console.WriteLine(link.InnerText);

                        System.Diagnostics.Debug.WriteLine(link.InnerText);

                        string href = link.GetAttribute("href");
                        string Filename = link.InnerText;
                        if (Filename != null)
                        {
                            if (Filename.Contains(".doc") == true)
                            {
                                  //webBrowser1.Navigate(href);

                                DownloadFileThread df = new DownloadFileThread();

                                df.DownloadFile(href,  Filename);

                                Console.WriteLine("Sleeping for 2 seconds");
                                System.Threading.Thread.Sleep(2000);

                                string FullPath = Application.StartupPath;

                                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                                proc.EnableRaisingEvents = false;
                                proc.StartInfo.FileName = "HNWDocParser.exe";
                                proc.StartInfo.Arguments = FullPath + @"\" + Filename;
                                // proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                proc.StartInfo.UseShellExecute = true;
                                proc.StartInfo.WorkingDirectory = FullPath;

                                proc.Start();
                                proc.WaitForExit();


                                this.Close();

                                try
                                {
                                   File.Delete( FullPath + @"\" + Filename);

                                }
                                catch { }

                                try
                                {
                                    File.Delete(FullPath + @"\" + Filename.ToLower().Replace(".doc",".pdf"));

                                }
                                catch { }


                                try
                                {
                                    File.Delete(HTMLFileName);

                                }
                                catch { }

                                ///WebClient webClient = new WebClient();

                                // Creates a webclient
                                //webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);                   // Uses the Event Handler to check whether the download is complete
                                //webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);  // Uses the Event Handler to check for progress made
                                //webClient.DownloadFileAsync(new Uri(href), @"C:\" + Filename);           // Defines the URL and destination directory for the downloaded file

                            }
                        }

                        


                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error downloading! " + ex.Message.ToString());
                    }

                }

            }


            // passwordEntry1

            System.Threading.Thread.Sleep(2000);

            webBrowser1.Focus();
            SendKeys.Send("{ENTER}");

            timer1.Enabled = true;
            ReadyForAttachment = true;

            return;

           // webBrowser1.Focus();
           // SendKeys.Send("{SPACE}");

            const uint WM_KEYDOWN = 0x100;

            //IntPtr hWndNotepad = FindWindow(null, "Form1");
            //if (!hWndNotepad.Equals(IntPtr.Zero))
            //{
            //   // IntPtr hWndEdit = FindWindowEx(hWndNotepad, 0, "Edit", null);
            //    //if (!hWndEdit.Equals(IntPtr.Zero))
            // //   {
            //        Keys key = Keys.Space;
            //        PostMessage(hWndNotepad, WM_KEYDOWN, (uint)key, 0);
            //   // }
            //}


            //HtmlDocument hd = webBrowser1.Document;//wbr is web browser control
            //HtmlElement he = hd.GetElementById("response_field");

            //he.InvokeMember("submit");


        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            RemoveBrowserKey();

        }

        private void webBrowser1_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            WebClient client = new WebClient();

            client.DownloadDataCompleted += new DownloadDataCompletedEventHandler(client_DownloadDataCompleted);

            client.DownloadDataAsync(e.Url);

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            webBrowser1.Focus();
            SendKeys.Send("{ENTER}");
        }
    }
}

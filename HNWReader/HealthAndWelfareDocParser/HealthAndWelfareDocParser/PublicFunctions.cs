﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Drawing;
using System.IO;
using System.Net.Mail;
using System.Net;


namespace MeridianPublicFunctions
{

    class PublicFunctions
    {

        string MAIL_SERVER = "smtp.meridiancity.org";


       



        public PublicFunctions()
        {



        }

        public bool SendMail(string from, string to, string CC,  string subject, bool isHTML, string messageContents)
        {

            try
            {
                MailMessage MTMail = new MailMessage();






                string[] users = to.Split(',');

                foreach (string user in users)
                {
                    MTMail.To.Add(new MailAddress(user + "@meridiancity.org"));


                }


                string[] CCUsers = CC.Split(',');

                try
                {

                    foreach (string user in CCUsers)
                    {
                        MTMail.CC.Add(new MailAddress(user + "@meridiancity.org"));


                    }

                }
                catch { }




                MTMail.From = new MailAddress(from + "@meridiancity.org");
                MTMail.Subject = subject;
                MTMail.IsBodyHtml = isHTML;


                MTMail.Body = messageContents;

               

                SmtpClient mailsender = new SmtpClient(MAIL_SERVER);

                mailsender.DeliveryMethod = SmtpDeliveryMethod.Network;
                mailsender.SendCompleted += new SendCompletedEventHandler(mailsender_SendCompleted);
                mailsender.Send(MTMail);



                
            }
            catch (Exception ex)
            {
                
                return false;
            }
            return true;



        }


        

        private void mailsender_SendCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
        {
            if (e.UserState is SmtpClient)
            {
                SmtpClient mailSender = ((SmtpClient)e.UserState);
                mailSender.SendCompleted -= mailsender_SendCompleted;
                mailSender = null;
            }

            if (e.Error != null)
            {
                //ogc.oError.HandleError("HDC-6806980", "cTickets-TicketEmailSent", "Error Sending Case Emails", "Error Sending Case Emails", e.Error);
            }
        }

       

       

      

      
        public String RemoveAllCharactersExceptNumbers(String InputString)
        {
            if (IsNumeric(InputString))
                return InputString;
            else
            {
                String str = "";
                Regex rgx = new Regex("[^0-9]");
                str = rgx.Replace(InputString, "");
                return str;
            }
        }


        
     
        public string FormatCitationNumber(string ADANo, string CitationNo)
        {
            int AdaLength = ADANo.Length;

            string NoPart = "000000000" + CitationNo;

            string Remaining = Right(NoPart, 10 - AdaLength);

            return ADANo + Remaining;

        }

   


        public bool IsDate(string Input, ref DateTime OutDate)
        {
            try
            {
                DateTime dt = DateTime.Parse(Input);
                OutDate = dt;
                return true;
            }
            catch
            {
                return false;
            }

        }


        public bool IsNumeric(string Input)
        {
            try
            {
                double i = double.Parse(Input);
                return true;
            }
            catch
            {
                return false;
            }

        }


        public string Left(string param, int length)
        {
            //we start at 0 since we want to get the characters starting from the
            //left and with the specified lenght and assign it to a variable


            if (length > param.Length)
                length = param.Length;


            string result = param.Substring(0, length);
            //return the result of the operation
            return result;
        }
        public string Right(string param, int length)
        {
            //start at the index based on the lenght of the sting minus
            //the specified lenght and assign it a variable
            string result = param.Substring(param.Length - length, length);
            //return the result of the operation
            return result;
        }

        public string Mid(string param, int startIndex, int length)
        {
            //start at the specified index in the string ang get N number of
            //characters depending on the lenght and assign it to a variable
            string result = param.Substring(startIndex, length);
            //return the result of the operation
            return result;
        }

        public string Mid(string param, int startIndex)
        {
            //start at the specified index and return all characters after it
            //and assign it to a variable
            string result = param.Substring(startIndex);
            //return the result of the operation
            return result;
        }


        public string FormatUSPhone(string num)
        {
            //first we must remove all non numeric characters
            num = num.Replace("(", "").Replace(")", "").Replace("-", "");

            // If no prefix is on it, add the 208 prefix
            if (num.Trim().Length == 7)
                num = "208" + num;



            string results = string.Empty;
            string formatPattern = @"(\d{3})(\d{3})(\d{4})";
            results = Regex.Replace(num, formatPattern, "($1) $2-$3");
            //now return the formatted phone number
            return results;
        }


       


        public string IIF(bool Input, string IfTrueValue, string IfFalseValue)
        {
            try
            {

                if (Input)
                    return IfTrueValue;
                else
                    return IfFalseValue;
            }
            catch
            {
                return IfFalseValue;


            }

        }

       


       












    }
}

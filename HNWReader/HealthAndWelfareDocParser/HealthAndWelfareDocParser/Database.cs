using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Collections;
using System.Windows.Forms;

namespace HealthAndWelfareDocParser
{
    // ----------------------------------------------------------------------------------------
    // Author:                    Mike Tanner
    // Company:                   
    // Assembly version:          1.0.0.0
    // Date:                      6/11/2008
    // Time:                      07:46
    // Class FullName:            DatabaseConnection.Database
    // Class Name:                Database
    // Class Kind Description:    Class
    // Class Kind Keyword:        class
    // Purpose:                   This is a database wrapper class
    // ----------------------------------------------------------------------------------------
    public class Database
    {
        public string serverValue="";
        private string databaseValue="";
        private string userValue = "";
        private string passwordValue = "";
        public  SqlConnection scon = null;




        public  Database(string server, string database, string user, string password)
        {
            try
            {
                this.serverValue = server;
                this.databaseValue = database;
                this.userValue = user;
                this.passwordValue = password;


                Console.WriteLine("Connecting to " + server);

                scon=GetConnection(scon);
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message.ToString());
               
            }

            



            


        }


        ~Database()
        {
            try
            {
                scon.Close();
            }
            catch 
            {

            }


        }

        public bool ExecDBInsertSignatureImage(byte[] Image, string ADANo,
                  string Password, ref string ErrorMessage)
        {
            try
            {
                string SQL = "";



                System.Data.SqlClient.SqlCommand dCMD = null;



                if (scon == null)
                    scon = OpenConnection(scon);


                if (!scon.State.Equals("Open"))
                {
                    scon = OpenConnection(scon);
                }






                if (Image != null)
                    SQL = "e_citation.dbo.upd_signature @worker_no ,@signature";

                // OleDbDataAdapter adapter = new OleDbDataAdapter(SQL, scon);



                dCMD = new System.Data.SqlClient.SqlCommand(SQL, scon);
                dCMD.CommandTimeout = 50000;



                if (Image != null)
                {

                    dCMD.Parameters.Add("@worker_no", System.Data.OleDb.OleDbType.Integer);

                    dCMD.Parameters["@worker_no"].Value = ADANo;

                    dCMD.Parameters.Add("@signature", System.Data.OleDb.OleDbType.Binary);

                    dCMD.Parameters["@signature"].Value = Image;
                }





                // returnValue = dCMD.ExecuteScalar().ToString();


                dCMD.ExecuteNonQuery();



                CloseDBConnection(scon);

                return true;

            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Cannot insert duplicate key") == true)
                {
                    ErrorMessage = ex.Message.ToString();
                    //System.Windows.Forms.MessageBox.Show("The value your trying to add already exists.");
                    return false;

                }
                else
                {
                    ErrorMessage = ex.Message.ToString();
                    //System.Windows.Forms.MessageBox.Show("Error inserting attachment! " + ex.Message.ToString());
                    return false;

                }


            }


            return true;



        }



        public SqlConnection OpenConnection(SqlConnection conn)
        {

            try
            {
                String strConnString = "server=" + serverValue.Trim() + ";uid=" + userValue.Trim() + ";pwd=" + passwordValue + ";database=" + databaseValue + ";" +
                                                            "connection reset = false;" + "connection lifetime=5;" + "min pool size=1;" + "max pool size=1000; Connect Timeout=130; ";
                conn = new SqlConnection(strConnString);

                //open and close a connection so that it is available in the pool
                conn.Open();

                

                return conn;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Database::getConnection: " + ex.ToString());
                return null ;
            }

           

        }

        public string GetServerName()
        {

            return serverValue;
        }



        public SqlConnection  GetConnection(SqlConnection  conn)
        {

            try
            {
                String strConnString = "server=" + serverValue.Trim() + ";uid=" + userValue.Trim() + ";pwd=" + passwordValue + ";database=" + databaseValue + ";" +
                                                            "connection reset = false;" + "connection lifetime=5;" + "min pool size=1;" + "max pool size=1000; Connect Timeout=30; ";
                conn = new SqlConnection(strConnString);
                
                //open and close a connection so that it is available in the pool
                conn.Open();
                conn.Close();


                return conn; 
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Error creating database connection to " + serverValue.Trim() + "! " + " Database::getConnection: " + ex.Message.ToString());
                return null;
            }
        }

        public bool CloseDBConnection(SqlConnection sConn)
        {
            bool blnRetVal = false;
            try
            {
                if (sConn.State.Equals("Open"))
                {
                    sConn.Close();
                }

           
                sConn.Close();
                sConn.Dispose();
                blnRetVal = true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Database::CloseDBConnection: " + ex.ToString());   
            }
            return (blnRetVal);


        }

       
        public DataSet ExecWithResultsDataset(string SQL, string datasetName)
        {

             DataSet ds = new DataSet();
            try
            {

                if (scon.State.Equals("Open"))
                {

                }
                else
                {
                    scon = OpenConnection(scon);
                }

                SqlDataAdapter adapter = new SqlDataAdapter(SQL, scon);

              
                

                adapter.Fill(ds,datasetName);
                

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.Message.ToString());
                return null;
            }

            CloseDBConnection(scon);

            return ds;



        }

        public string ExecWithSingleResult(string SQL)
        {

            string returnValue = null;

            try
            {
                System.Data.SqlClient.SqlCommand dCMD = null;

                if (scon == null)
                    scon = OpenConnection(scon);

                if (scon.State.ToString() == "Open") 
                {

                }
                else
                {
                    scon=OpenConnection(scon);
                }


                



                dCMD = new System.Data.SqlClient.SqlCommand(SQL, scon);

                dCMD.CommandTimeout = 60000;
                
                returnValue = dCMD.ExecuteScalar().ToString();

                CloseDBConnection(scon);
                
            }
            catch (Exception ex)
            {
                //System.Windows.Forms.MessageBox.Show(ex.Message.ToString());
                return "";
            }

            return returnValue;


        }

        public string ExecWithSingleResult(string SQL, ref string errorMessage)
        {

            string returnValue = null;

            try
            {
                System.Data.SqlClient.SqlCommand dCMD = null;


                if (scon.State.ToString() == "Open")
                {

                }
                else
                {
                    scon = OpenConnection(scon);
                }

                dCMD = new System.Data.SqlClient.SqlCommand(SQL, scon);


                returnValue = dCMD.ExecuteScalar().ToString();

                CloseDBConnection(scon);

            }
            catch (Exception ex)
            {

                errorMessage = ex.Message.ToString();
                return "";
            }

            return returnValue;


        }

        public bool ExecDBInsert(string SQL)
        {

            try
            {
	            System.Data.SqlClient.SqlCommand dCMD = null;

                        if (scon.State.ToString() =="Open")
                        {

                        }
                        else
                        {
                            scon = OpenConnection(scon);
                        }


                        dCMD = new System.Data.SqlClient.SqlCommand(SQL, scon);
                        dCMD.ExecuteNonQuery();
                        CloseDBConnection(scon);

            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Cannot insert duplicate key") == true)
                {
                    System.Windows.Forms.MessageBox.Show("The value your trying to add already exists.");
                    return false;

                }
                else
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message.ToString());
                    return false;

                }
                
                
            }


            return true;



        }

        public bool ExecDBInsertAttachmentImage(byte[] Image, byte[] ThumbnailImage, ref string attachment_GUID, string AttachmentType, 
                    string FileTitle, string FileName, string Extension, string FileSizeInbytes, string FileCreationDatetime, 
                    string FileComment, string Worker_No, ref string ErrorMessage)
        {
            try
            {
                System.Data.SqlClient.SqlCommand dCMD = null;

                if (scon.State.ToString() == "Open")
                {

                }
                else
                {
                    scon = OpenConnection(scon);
                }


                string SQL="";

                 if (ThumbnailImage != null)
                     SQL = "exec ins_attachment @attachment_type,@file_title,@file_name,@extension,@file_size_in_kbytes,@file_attachment,@file_creation_datetime,@added_by_worker_no,@file_comment,@thumbnail_image";
                 else
                     SQL = "exec ins_attachment @attachment_type,@file_title,@file_name,@extension,@file_size_in_kbytes,@file_attachment,@file_creation_datetime,@added_by_worker_no,@file_comment";



                dCMD = new System.Data.SqlClient.SqlCommand(SQL, scon);
                dCMD.CommandTimeout = 50000;


                dCMD.Parameters.Add("@attachment_type", SqlDbType.VarChar);
                dCMD.Parameters.Add("@file_title", SqlDbType.VarChar);
                dCMD.Parameters.Add("@file_name", SqlDbType.VarChar);
                dCMD.Parameters.Add("@extension", SqlDbType.VarChar);
                dCMD.Parameters.Add("@file_size_in_kbytes", SqlDbType.BigInt);
                dCMD.Parameters.Add("@file_attachment", SqlDbType.VarBinary);
                dCMD.Parameters.Add("@file_creation_datetime", SqlDbType.DateTime );
                dCMD.Parameters.Add("@added_by_worker_no", SqlDbType.Int);
                dCMD.Parameters.Add("@file_comment", SqlDbType.VarChar);

                if (ThumbnailImage != null)
                    dCMD.Parameters.Add("@thumbnail_image", SqlDbType.VarBinary   );




                dCMD.Parameters["@attachment_type"].Value = AttachmentType;
                dCMD.Parameters["@file_title"].Value = FileTitle;
                dCMD.Parameters["@file_name"].Value = FileName;
                dCMD.Parameters["@extension"].Value = Extension;
                dCMD.Parameters["@file_size_in_kbytes"].Value = FileSizeInbytes;
                dCMD.Parameters["@file_attachment"].Value = Image;
                dCMD.Parameters["@file_creation_datetime"].Value = FileCreationDatetime;
                dCMD.Parameters["@added_by_worker_no"].Value = Worker_No;
                dCMD.Parameters["@file_comment"].Value = FileComment;

                if (ThumbnailImage != null)
                    dCMD.Parameters["@thumbnail_image"].Value = ThumbnailImage;



                string returnValue;


                returnValue = dCMD.ExecuteScalar().ToString();

                attachment_GUID = returnValue; 


                              
                CloseDBConnection(scon);

                return true;

            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Cannot insert duplicate key") == true)
                {
                    ErrorMessage = ex.Message.ToString();
                    //System.Windows.Forms.MessageBox.Show("The value your trying to add already exists.");
                    return false;

                }
                else
                {
                    ErrorMessage = ex.Message.ToString();
                    //System.Windows.Forms.MessageBox.Show("Error inserting attachment! " + ex.Message.ToString());
                    return false;

                }


            }


            return true;



        }



        public bool ExecDBInsertImage(string SQL, byte[] image, string FieldName, string FieldValue)
        {

            try
            {
                System.Data.SqlClient.SqlCommand dCMD = null;

                if (scon.State.ToString() == "Open")
                {

                }
                else
                {
                    scon = OpenConnection(scon);
                }


             
                dCMD = new System.Data.SqlClient.SqlCommand(SQL, scon);


                dCMD.Parameters.Add("@contact_id", SqlDbType.Int );


               


                dCMD.Parameters.Add("@image_data", SqlDbType.Image );

                
                dCMD.Parameters["@image_data"].Value = image;
                dCMD.Parameters[FieldName].Value = FieldValue;


                dCMD.ExecuteNonQuery();
                CloseDBConnection(scon);

            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Cannot insert duplicate key") == true)
                {
                    System.Windows.Forms.MessageBox.Show("The value your trying to add already exists.");
                    return false;

                }
                else
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message.ToString());
                    return false;

                }

               
            }


            return true;



        }



        public bool ExecDBBinary(string SQL, byte[] image)
        {

            try
            {
                System.Data.SqlClient.SqlCommand dCMD = null;

                if (scon.State.ToString() == "Open")
                {

                }
                else
                {
                    scon = OpenConnection(scon);
                }




                dCMD = new System.Data.SqlClient.SqlCommand(SQL, scon);

                dCMD.Parameters.Add("@file_attachment", SqlDbType.VarBinary   );


                dCMD.Parameters["@file_attachment"].Value = image;


                dCMD.ExecuteNonQuery();
                CloseDBConnection(scon);

            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Cannot insert duplicate key") == true)
                {
                    System.Windows.Forms.MessageBox.Show("The value your trying to add already exists.");
                    return false;

                }
                else
                {
                    System.Windows.Forms.MessageBox.Show(ex.Message.ToString());
                    return false;

                }


            }


            return true;



        }

        public bool ExecDBInsert(string SQL, ref string outErrorMessage)
        {

            try
            {

               // MessageBox.Show(SQL);

                System.Data.SqlClient.SqlCommand dCMD = null;

                if (scon.State.ToString() == "Open")
                {

                }
                else
                {
                    scon = OpenConnection(scon);
                }


                dCMD = new System.Data.SqlClient.SqlCommand(SQL, scon);
                dCMD.ExecuteNonQuery();
                CloseDBConnection(scon);

            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Cannot insert duplicate key") == true)
                {
                    outErrorMessage = "Cannot insert duplicate Value!";
                    return false;

                }
                else
                {
                    outErrorMessage = ex.Message.ToString(); 
                    //System.Windows.Forms.MessageBox.Show(ex.Message.ToString());
                    return false;

                }

                
            }


            return true;



        }

        public bool ExecDBInsertLeaveOpen(string SQL, ref string outErrorMessage)
        {

            try
            {
                System.Data.SqlClient.SqlCommand dCMD = null;

                if (scon.State.ToString() == "Open")
                {

                }
                else
                {
                    scon = OpenConnection(scon);
                }


                dCMD = new System.Data.SqlClient.SqlCommand(SQL, scon);
                dCMD.ExecuteNonQuery();
               // CloseDBConnection(scon);

            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("Cannot insert duplicate key") == true)
                {
                    outErrorMessage = "Cannot insert duplicate Value!";
                    return false;

                }
                else
                {
                    outErrorMessage = ex.Message.ToString();
                    //System.Windows.Forms.MessageBox.Show(ex.Message.ToString());
                    return false;

                }


            }


            return true;



        }



        public DataView  ExecWithResultsDataView(string SQL)
        {
            TryAgain:

            DataSet ds = new DataSet();
            try
            {

               


                if (scon.State.Equals("Open"))
                {

                }
                else
                {
                    scon = OpenConnection(scon);
                }



                System.Data.SqlClient.SqlCommand dCMD = new SqlCommand(SQL, scon);
                dCMD.CommandTimeout = 12000;

                SqlDataAdapter adapter = new SqlDataAdapter(dCMD);

                adapter.Fill(ds);


            }
            catch (Exception ex)
            {

                if (ex.Message.ToString().Contains("System.OutOfMemoryException") == true)
                {

                    System.Threading.Thread.Sleep(2000);
                    goto TryAgain;
                }


                System.Windows.Forms.MessageBox.Show(ex.Message.ToString());
                return null;
            }

            DataView dv = new DataView(ds.Tables[0]);

            CloseDBConnection(scon);



            return dv;



        }

        public DataView ExecWithResultsDataViewSilent(string SQL)
        {
        TryAgain:

            DataSet ds = new DataSet();
            try
            {




                if (scon.State.Equals("Open"))
                {

                }
                else
                {
                    scon = OpenConnection(scon);
                }



                System.Data.SqlClient.SqlCommand dCMD = new SqlCommand(SQL, scon);
                dCMD.CommandTimeout = 12000;

                SqlDataAdapter adapter = new SqlDataAdapter(dCMD);

                adapter.Fill(ds);


            }
            catch (Exception ex)
            {

                if (ex.Message.ToString().Contains("System.OutOfMemoryException") == true)
                {

                    System.Threading.Thread.Sleep(2000);
                    goto TryAgain;
                }


                //System.Windows.Forms.MessageBox.Show(ex.Message.ToString());
                return null;
            }

            DataView dv = new DataView(ds.Tables[0]);

            CloseDBConnection(scon);



            return dv;



        }

        public DataView ExecWithResultsDataViewLeaveOpen(string SQL)
        {
        TryAgain:

            DataSet ds = new DataSet();
            try
            {




              



                System.Data.SqlClient.SqlCommand dCMD = new SqlCommand(SQL, scon);
                dCMD.CommandTimeout = 12000;

                SqlDataAdapter adapter = new SqlDataAdapter(dCMD);

                adapter.Fill(ds);


            }
            catch (Exception ex)
            {

                if (ex.Message.ToString().Contains("System.OutOfMemoryException") == true)
                {

                    System.Threading.Thread.Sleep(2000);
                    goto TryAgain;
                }


                System.Windows.Forms.MessageBox.Show(ex.Message.ToString());
                return null;
            }

            DataView dv = new DataView(ds.Tables[0]);

         


            return dv;



        }

   


    }
    
}

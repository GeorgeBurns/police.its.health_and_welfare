﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using GdPicture10;
using MeridianPublicFunctions;
using System.Runtime.InteropServices.ComTypes;
using Microsoft.Office.Interop.Word;
using System.Text.RegularExpressions;
using System.IO;


namespace HealthAndWelfareDocParser
{
    public partial class Form1 : Form
    {
        GdPictureImaging oGdPictureImaging = new GdPictureImaging();
        GdPicturePDF pdf = new GdPicturePDF();
        GdPictureStatus Status = GdPictureStatus.OK;
        PublicFunctions pf = new PublicFunctions();
        string TempDir = Path.GetTempPath();
        Database DataDB = new Database(@"SQLPOLICE\POLICE", "incident_tracking", "H_W_IMPORTER", "H_W_IMPORTER1@");
        string BingLicenseKey = "AgDfeMI7LLO1QZMgPaPsCHBqxzljr0L70Uy9ShMRZkZJafk-E-FiguOseDRiIcjp";

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //MessageBox.Show("For, load");
        }

        private void btnImportFile_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fd = new OpenFileDialog();
                fd.RestoreDirectory = true;
                fd.Filter = "Word Files (.doc)|*.doc|All Files (*.*)|*.*";

                fd.ShowDialog();


                string Filename = fd.FileName;


                if (Filename == "")
                {


                    return;
                }

                System.Windows.Forms.Application.DoEvents();
                lblImporting.Visible = true;

                System.Windows.Forms.Application.DoEvents();
               

                PublicFunctions pf = new PublicFunctions();
                string ReferralDate = "";
                string FamilyName = "";
                string ReportTakenBy = "";
                string RiskPriority = "";
                string PINo = "";
                string ReferralReason = "";
                string IssueLocation = "";
                string Narrative = "";
                string CurrentAssignedStaff = "";
                
                string ReferentRelationship = "";
                string ReferentSource = "";
                string ReferentAddress = "";
                string ReferentPhone = "";


                string FamilyAddress = "";
                string FamilyHomePhone = "";
                string FamilyWorkPhone = "";
                string FamilyCityStateZip = "";





                try
                {

                    oGdPictureImaging.SetLicenseNumber("5329779344795307267332154"); // Please, replace XXXX by a valid demo or commercial license key. 
                    pdf.SetLicenseNumber("7263229428892987466341972");

                    GdPicture10.LicenseManager lm = new GdPicture10.LicenseManager();
                    lm.RegisterKEY("923298485935197751413112040111476"); // Real license key
                    lm.RegisterKEY("726322766916098671420143053635028");
                    lm.RegisterKEY("43158441615652134819");
                    lm.RegisterKEY("05552901357999941237");
                    lm.RegisterKEY("923298485935197751413112040111476");
                    lm.RegisterKEY("826314899966255751216122495541633");



                    
                  //  lb.Items.Add("HealthAndWelfareDocParser by Mike Tanner");
                   // lb.Items.Add("----------------------------------------");


                    lb.Items.Add("");

                    lb.TopIndex = lb.Items.Count - 1;
                    System.Windows.Forms.Application.DoEvents();
                   






                    lb.Items.Add("Opening MS Word");
                    lb.TopIndex = lb.Items.Count - 1;
                    System.Windows.Forms.Application.DoEvents();
                    System.Windows.Forms.Application.DoEvents();
                    Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();
                    System.Windows.Forms.Application.DoEvents();


                    lb.Items.Add("Opening " + Filename);
                    lb.TopIndex = lb.Items.Count - 1;
                    System.Windows.Forms.Application.DoEvents();
                    System.Windows.Forms.Application.DoEvents();
                    Document doc = word.Documents.Open(Filename);


                    word.ShowMe();
                    word.Visible = true;


                    lb.Items.Add("Starting Parsing magic...");
                    lb.TopIndex = lb.Items.Count - 1;
                    System.Windows.Forms.Application.DoEvents();
                    System.Windows.Forms.Application.DoEvents();

                    string text = doc.Range().Text;

                    char[] delimiterChars = { '\r', '\n' };


                    string[] Splitter = text.Split(delimiterChars, StringSplitOptions.RemoveEmptyEntries);



                    for (int x = 0; x <= Splitter.Length - 1; x++)
                    {
                        string Input = Splitter[x];

                        string[] Splitter2;

                        lb.Items.Add(Input);

                        if (Input.Contains("Referral Date:") == true)
                        {
                            Splitter2 = Input.Split(':');
                            ReferralDate = Splitter2[1].ToString().Trim();
                        }
                        if (Input.Contains("Referral Time:") == true)
                        {
                            Splitter2 = Input.Split(':');
                            ReferralDate = ReferralDate + " " + Splitter2[1].ToString().Trim() + ":" + Splitter2[2].ToString().Trim();
                        }
                        else if (Input.Contains("Family Name:") == true)
                        {
                            Splitter2 = Input.Split(':');
                            FamilyName = Splitter2[1].ToString().Trim();
                        }
                        else if (Input.Contains("Report Taken By:") == true)
                        {
                            Splitter2 = Input.Split(':');
                            ReportTakenBy = Splitter2[1].ToString().Trim();
                        }
                        else if (Input.Contains("Risk Priority:") == true)
                        {
                            Splitter2 = Input.Split(':');
                            RiskPriority = Splitter2[1].ToString().Trim();
                        }
                        else if (Input.Contains("Presenting Issue #:") == true)
                        {
                            Splitter2 = Input.Split(':');
                            PINo = Splitter2[1].ToString().Trim();
                        }
                        else if (Input.Contains("Referral Reason:") == true)
                        {
                            Splitter2 = Input.Split(':');
                            ReferralReason = Splitter2[1].ToString().Trim();
                        }
                        else if (Input.Contains("Issue Location:") == true)
                        {
                            Splitter2 = Input.Split(':');
                            IssueLocation = Splitter2[1].ToString().Trim();
                        }
                        else if (Input.Contains("Issue Description:") == true)
                        {
                            Splitter2 = Input.Split(':');


                            int StartOfNarrative = text.IndexOf("Issue Description:") + 19;
                            int EndOfNarrative = text.IndexOf("Response Date/Time") + 1;

                            Narrative = Mid(text, StartOfNarrative, EndOfNarrative - StartOfNarrative).Trim();



                        }
                        else if (Input.Contains("Current Staff Assigned:") == true)
                        {
                            try
                            {
                                Splitter2 = Input.Split(':');


                                CurrentAssignedStaff = Splitter2[Splitter2.Length - 1].ToString().Trim();
                            }
                            catch { }

                        }



                        // Current Staff Assigned

                    }


                    // Get Family Address

                    bool IntakeWorksheet_found = false;
                    int TableCounter = 0;




                    FamilyAddress = doc.Tables[3].Cell(1, 0).Range.Text.Trim().Replace("\r\a", "");

                    string[] faSplit = FamilyAddress.Split('\r');

                    try
                    {
                        FamilyCityStateZip = faSplit[faSplit.Length - 1].ToString();

                    }
                    catch { }

                    FamilyHomePhone = doc.Tables[3].Cell(1, 2).Range.Text.Trim().Replace("\r\a", "");
                    try
                    {
                        string[] Splitter4 = FamilyHomePhone.Split(':');
                        FamilyHomePhone = Splitter4[1].ToString().Trim();

                    }
                    catch { }


                    FamilyWorkPhone = doc.Tables[3].Cell(1, 3).Range.Text.Trim().Replace("\r\a", "");
                    try
                    {
                        string[] Splitter4 = FamilyWorkPhone.Split(':');
                        FamilyWorkPhone = Splitter4[1].ToString().Trim();

                    }
                    catch { }


                    bool referent_found = false;
                    TableCounter = 0;

                    foreach (Table tb in doc.Tables)
                    {
                        foreach (Row r in tb.Rows)
                        {
                            foreach (Cell c in r.Cells)
                            {
                                lb.Items.Add(c.Range.Text);

                                if (c.Range.Text.Contains("Referent Information"))
                                {
                                    referent_found = true;
                                    break;
                                }

                            }
                            if (referent_found == true)
                                break;
                        }

                        if (referent_found == true)
                            break;


                        TableCounter++;
                    }


                    int RowCounter = 1;

                    if (referent_found == true)
                    {

                        // foreach (Row r in doc.Tables[TableCounter + 2].Rows)
                        //{
                        ReferentRelationship = doc.Tables[TableCounter + 2].Cell(1, 0).Range.Text.Trim().Replace("\r\a", "");

                        try
                        {
                            string[] Splitter3 = ReferentRelationship.Split(':');
                            ReferentRelationship = Splitter3[1].ToString().Trim();
                        }
                        catch { }


                        ReferentSource = doc.Tables[TableCounter + 2].Cell(1, 2).Range.Text.Trim().Replace("\r\a", "");

                        try
                        {
                            string[] Splitter3 = ReferentSource.Split(':');
                            ReferentSource = Splitter3[1].ToString().Trim();
                        }
                        catch { }

                        ReferentAddress = doc.Tables[TableCounter + 2].Cell(2, 0).Range.Text.Trim().Replace("\r\a", "").Trim();
                        ReferentPhone = doc.Tables[TableCounter + 2].Cell(2, 2).Range.Text.Trim().Replace("\r\a", "").Trim();


                        //}
                    }

                    lb.Items.Add("Got incident data, now inserting..");
                    lb.TopIndex = lb.Items.Count - 1;
                    System.Windows.Forms.Application.DoEvents();
                    System.Windows.Forms.Application.DoEvents();

                    string ErrroMessage = "";

                    lb.Items.Add("Running Address Through Bing..");
                    lb.TopIndex = lb.Items.Count - 1;
                    System.Windows.Forms.Application.DoEvents();
                    string[] AddressSplitter = FamilyAddress.Split('\r');

                    string FamilyAddressProper = AddressSplitter[0].ToString();

                    string RD = "";

                    if (FamilyAddressProper.Trim().Length > 5)
                        RD = GetRDFromAddress(FamilyAddressProper);

                    if (DataDB.ExecDBInsert("ins_health_and_welfare_referral " + PINo + ",'" + RiskPriority.Replace("'", "''") + "','" + ReferralReason.Replace("'", "''") + "','" + ReferralDate + "','" +
                        FamilyName.Replace("'", "''") + "','" + IssueLocation.Replace("'", "''") + "','" + Narrative.Replace("'", "''") + "','Y',0,'" + CurrentAssignedStaff.Replace("'", "''") + "','" + ReferentRelationship.Replace("'", "''") +
                        "','" + ReferentAddress.Replace("'", "''") + "','" + ReferentSource.Replace("'", "''") + "','" + ReferentPhone.Replace("'", "''") + "','" + FamilyAddress.Replace("'", "''") + "','" +
                         FamilyHomePhone.Replace("'", "''") + "','" + FamilyWorkPhone.Replace("'", "''") + "','" + RD + "'", ref ErrroMessage) == false)
                    {
                        pf.SendMail("HNWDocParser", "hwreferrals", "", "Failed to insert HW referral! " + PINo, true, ErrroMessage);
                        return;

                    }



                    string incident_no = DataDB.ExecWithSingleResult("select incident_no from health_and_welfare_referral where PI_no = " + PINo.ToString());


                    // Insert into Incident Address


                    string ErrorMessge = "";



                    string bing_out = "";


                    if (FamilyAddressProper.Trim().Length > 5)
                    {
                        BingMaps bm = new BingMaps(BingLicenseKey);
                        bing_out = bm.GeocodeAddressString(FamilyAddressProper + " " + FamilyCityStateZip);
                    }
                    lb.Items.Add("Found " + bing_out);
                    lb.TopIndex = lb.Items.Count - 1;
                    System.Windows.Forms.Application.DoEvents();

                    if (bing_out.Contains(",") == true)
                    {

                        string[] BingSplitter = bing_out.Split(',');
                        string latitude = BingSplitter[0];
                        string longitude = BingSplitter[1];
                        string zip_code = BingSplitter[2];
                        string city = BingSplitter[3];
                        string binged_address = BingSplitter[4];

                        string[] AddressSplitterFromBing = binged_address.Split(' ');

                        string AddressNO = AddressSplitterFromBing[0];
                        string PreDire = AddressSplitterFromBing[1];
                        string Suffix = AddressSplitterFromBing[AddressSplitterFromBing.Length - 1];

                        string StreetName = "";
                        if (AddressSplitterFromBing.Length == 4)
                        {
                            StreetName = AddressSplitterFromBing[2];

                        }
                        else if (AddressSplitterFromBing.Length == 5)
                        {
                            StreetName = AddressSplitterFromBing[2] + AddressSplitterFromBing[3];

                        }

                        string ErrorMessage5 = "";

                        if (DataDB.ExecDBInsert("ins_incident_address " + incident_no + "," + AddressNO + ",'" + PreDire.Trim() + "','" + StreetName.Trim().Replace("'", "''") +
                            "','" + Suffix.Trim().Replace("'", "''") + "','" + city.Trim().Replace("'", "''") + "','" + "" + "','" + "" +
                            "','" + "" + "'," + zip_code + ",'" + "ADDRESS" + "','" +
                            "" + "'," + latitude + "," + longitude + ",'" + "ID" + "'", ref ErrorMessage5) == false)
                        {

                            pf.SendMail("HNWDocParser", "hwreferrals", "", "Failed to Address to  HW referral! " + PINo, true, ErrorMessage5);

                            return;
                        }




                    }
                    else
                    {

                        string ErrorMessage6 = "";

                        if (DataDB.ExecDBInsert("ins_incident_address " + incident_no + "," + "null" + ",'" + "" + "','" + FamilyAddressProper.Trim().Replace("'", "''") +
                           "','" + "" + "','" + "MERIDIAN" + "','" + "" + "','" + "" +
                           "','" + "" + "'," + "86642" + ",'" + "ADDRESS" + "','" +
                           "" + "'," + "NULL" + "," + "NULL" + ",'" + "ID" + "'", ref ErrorMessage6) == false)
                        {

                            pf.SendMail("HNWDocParser", "hwreferrals", "", "Failed to Address to  HW referral! " + PINo, true, ErrorMessage6);

                            return;
                        }



                    }

                    string BusinessName = "";
                    string[] Bus = ReferentAddress.Split('\r');
                    string CityStateZip = "";



                    try
                    {
                        BusinessName = Bus[0].ToString().Trim();
                        ReferentAddress = Bus[1].ToString();

                        if (ReferentAddress.Trim() == "" && BusinessName.Trim().Length > 0)
                        {
                            ReferentAddress = BusinessName;
                            BusinessName = "";
                        }
                        CityStateZip = Bus[2].ToString();

                    }
                    catch { }

                    string ContactID = InsertContactIntoITS(ReferentSource.Replace("'", "''"), "", "", "", "", "", "-1", "Reporting Party", "", ReferentPhone, "", true, PINo);
                    SaveContactAddress(ContactID, ReferentAddress, BusinessName, ReferentAddress, CityStateZip);

                    string SQL = "ins_health_and_welfare_referral_contact " + PINo + ",-1," + ContactID + "," + incident_no;

                    if (DataDB.ExecDBInsert(SQL, ref ErrorMessge) == false)
                    {

                        pf.SendMail("HNWDocParser", "hwreferrals", "", "Failed to insert HW referral contact! " + PINo + "  SQL = " + SQL, true, ErrorMessge);
                        // return;

                    }

                    bool COCFound = false;
                    TableCounter = 0;

                    foreach (Table tb in doc.Tables)
                    {
                        foreach (Row r in tb.Rows)
                        {
                            foreach (Cell c in r.Cells)
                            {
                                lb.Items.Add(c.Range.Text);

                                if (c.Range.Text.Contains("Presenting Issue Participants"))
                                {
                                    COCFound = true;
                                    break;
                                }

                            }
                            if (COCFound == true)
                                break;
                        }

                        if (COCFound == true)
                            break;


                        TableCounter++;
                    }


                    lb.Items.Add("Getting contacts");
                    lb.TopIndex = lb.Items.Count - 1;
                    System.Windows.Forms.Application.DoEvents();


                    RowCounter = 1;
                    if (COCFound == true)
                    {
                        foreach (Row r in doc.Tables[TableCounter + 2].Rows)
                        {
                            string Name = "";
                            string SSN = "";
                            string Role = "";
                            string Relationship = "";
                            string DOB = "";
                            string ethnicity = "";
                            string Race = "";
                            string Gender = "";
                            string ClientID = "";
                            string Age = "0";




                            if (RowCounter > 1)
                            {

                                Name = doc.Tables[TableCounter + 2].Cell(RowCounter, 0).Range.Text.Trim().Replace("\r\a", "");


                                if (Name.Contains("\r"))
                                {
                                    string[] NameSplitter = Name.Split('\r');
                                    Name = NameSplitter[0].Trim();

                                }

                                ClientID = doc.Tables[TableCounter + 2].Cell(RowCounter, 2).Range.Text.Trim().Replace("\r\a", "");
                                SSN = doc.Tables[TableCounter + 2].Cell(RowCounter, 3).Range.Text.Trim().Replace("\r\a", "");
                                Role = doc.Tables[TableCounter + 2].Cell(RowCounter, 4).Range.Text.Trim().Replace("\r\a", "");



                                GetFamilyProfile(doc, TableCounter + 4, Name, out Gender, out Relationship, out DOB, out ethnicity, out Race);


                                Age = CalculateAge(DOB).ToString();

                                lb.Items.Add("Inserting " + Name);
                                lb.TopIndex = lb.Items.Count - 1;
                                System.Windows.Forms.Application.DoEvents();


                                string ContactID2 = InsertContactIntoITS(Name, DOB, Gender, ethnicity, Race, SSN, ClientID, Role, FamilyHomePhone, "", Age, false, PINo);

                                SaveContactAddress(ContactID2, FamilyAddress, "", "", FamilyCityStateZip);



                                if (DataDB.ExecDBInsert("ins_health_and_welfare_referral_contact " + PINo + "," + ClientID + "," + ContactID2 + "," + incident_no, ref ErrorMessge) == false)
                                {

                                    pf.SendMail("HNWDocParser", "hwreferrals", "", "Failed to insert HW referral contact! " + PINo, true, ErrorMessge);
                                    // return;

                                }




                            }

                            RowCounter++;
                        }
                    }





                    // COnvert to pdf
                    object outputFileName = doc.FullName.Replace(".doc", ".pdf");
                    object fileFormat = WdSaveFormat.wdFormatPDF;
                    doc.SaveAs(outputFileName, ref fileFormat);


                    doc.Close(false);
                    word.Quit(false);


                    System.Threading.Thread.Sleep(1000);

                    // Get Incident No


                    string ImagesPath = "";
                    string ErrorMessagePDF = "";
                    // string Attachment_GUID = "";


                    if (BustPDFIntoImagesGD(outputFileName.ToString(), ref ImagesPath, ref ErrorMessagePDF) == false)
                    {
                        //lstStatus.Items.Insert(0, "Error converting to image! " + ErrorMessagePDF);
                        MessageBox.Show("Error converting to pdf! " + ErrorMessagePDF);


                    }



                    string IncidentNo = DataDB.ExecWithSingleResult("select incident_no from health_and_welfare_referral where PI_no=" + PINo);



                    string ErrorMessageFromID = "";
                    string incident_support_doc_id = DataDB.ExecWithSingleResult("ins_incident_support_document '" + "H&W Referral # " + PINo + " " + FamilyName.Replace("'", "''") + "','" + "" + "'," + "0" + ",'" + "8x10" + "'", ref ErrorMessageFromID);





                    // Process Each Image

                    foreach (string f in Directory.GetFiles(ImagesPath, "*.tiff"))
                    {
                        try
                        {
                            string Attachment_GUID = "";
                            string PageNo = "";
                            string ConversionError = "";

                            if (ConvertImage(f, incident_support_doc_id, ref Attachment_GUID, ref PageNo, "H&W Referral # " + PINo + " " + FamilyName.Replace("'", "''"), "", ref ConversionError) == false)
                            {
                                // lstStatus.Items.Insert(0, "Converting tif to jpg! " + ConversionError);
                                //Application.DoEvents();
                            }
                            else
                            {


                                string InsertError = "";
                                if (DataDB.ExecDBInsert("ins_incident_support_document_attachment " + incident_support_doc_id + ",'" + Attachment_GUID + "'," + PageNo + "," + "0", ref InsertError) == false)
                                {
                                    // lstStatus.Items.Insert(0, "Error attaching document to support doc " + InsertError);
                                    //Application.DoEvents();

                                }


                            }
                        }
                        catch { }
                    }

                    string ErrorMessageLast = "";

                    if (DataDB.ExecDBInsert("ins_incident_support_doc_assoc " + IncidentNo + "," + incident_support_doc_id + "," + "0" + ",'" + "Y" + "','N'", ref ErrorMessageLast) == false || ErrorMessageLast.Length > 0)
                    {

                        MessageBox.Show("Error associating support doc to incident! " + ErrorMessageLast);

                        return;

                    }


                    try
                    {
                        File.Delete(outputFileName.ToString());
                    }
                    catch (Exception ex)
                    {

                    }

                    byte[] OtherDoc;
                    //string Attachment_GUID = "";
                    long FileSizeInBytes = 0;
                    string ErrorMessage = "";
                    string FileCreationDate = "";

                    //FileInfo fi = new FileInfo(Filename);

                    //string Extension = fi.Extension.ToLower();
                    //FileCreationDate = fi.LastWriteTime.ToString("MM/dd/yyyy HH:mm:ss");


                    //FileStream fs = new FileStream(fi.FullName, FileMode.Open);

                    //BinaryReader reader = new BinaryReader(fs);
                    //OtherDoc = reader.ReadBytes((int)fs.Length);


                    //fs.Close();

                    ////ReferralReason.ToString();

                    //FileSizeInBytes = fi.Length / 1024;

                    //if (DataDB.ExecDBInsertAttachmentImage(OtherDoc, null, ref Attachment_GUID, "OTHER",   "H&W Referral # " + PINo + " " + FamilyName.Replace("'", "''") ,
                    //  fi.Name.Trim().Replace("'", "''"), fi.Extension.Trim().Replace("'", "''"), FileSizeInBytes.ToString(), FileCreationDate,
                    //"", "0", ref ErrorMessage) == false)
                    //{
                    //    pf.SendMail("HNWDocParser", "mtanner", "", PINo + " failed on inserting attachment! " + ErrorMessage, true, "PI# " + PINo + " failed on inserting attachment! " + ErrorMessage);

                    //}

                    //if (DataDB.ExecDBInsert("ins_incident_attachment " + IncidentNo + ",'" + Attachment_GUID + "'," + "0", ref ErrorMessage) == false)
                    //{
                    //    pf.SendMail("HNWDocParser", "mtanner", "", PINo + " failed on attachment association of " + Attachment_GUID + "! " + ErrorMessage, true, "PI# " + PINo + " failed on attachment association! of " + Attachment_GUID + ErrorMessage);
                    //}


                    // Insert document into database

                    pf.SendMail("HNWDocParser", "hwreferrals", "", PINo + " was imported", true, "PI# " + PINo + " has been imported! </br>Family: " + FamilyName.Replace("'", "''"));


                    lb.TopIndex = lb.Items.Count - 1;

                    Cursor = Cursors.Default;


                    MessageBox.Show("Imported!");
                      this.Close();


                }
                catch (Exception ex)
                {
                    pf.SendMail("HNWDocParser", "hwreferrals", "", "Failed to insert HW referral contact! " + PINo, true, "Something went wrong!!  " + ex.Message.ToString());


                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Uh oh, something went wrong.. " + ex.Message.ToString());

            }



        }

        private void ExtractAddressPiecesFromAddress(string Address, out string txtStreetNo, out string cboAddressDirection, out string txtStreetNameAddress, out string cboAddressSuffix, out string txtCity)
        {

            txtStreetNo = "";
            cboAddressDirection = "";
            txtStreetNameAddress = "";
            cboAddressSuffix = "";
            txtCity = "";

            try
            {
                Address = Address.Replace("    ", " ");
                Address = Address.Replace("   ", " ");
                Address = Address.Replace("  ", " ");



                string[] Splitter = Address.Split('\r');
                string AddressLine1 = Splitter[0].ToString().Trim();
                string[] AddressSplitter = AddressLine1.Split(' ');

                if (AddressSplitter.Length == 4)
                {
                    txtStreetNo = AddressSplitter[0].ToString();
                    cboAddressDirection = AddressSplitter[1].ToString();
                    txtStreetNameAddress = AddressSplitter[2].ToString();
                    cboAddressSuffix = AddressSplitter[3].ToString();

                    txtCity = "Meridian";

                }
                else if (AddressSplitter.Length == 3)
                {
                    txtStreetNo = AddressSplitter[0].ToString();
                    cboAddressDirection = AddressSplitter[1].ToString();


                    if (cboAddressDirection.Length > 2)
                    {
                        cboAddressDirection = "";
                        txtStreetNameAddress = AddressSplitter[1].ToString();
                        cboAddressSuffix = AddressSplitter[2].ToString();


                    }
                    else
                    {

                        txtStreetNameAddress = AddressSplitter[1].ToString();
                        cboAddressSuffix = AddressSplitter[2].ToString();
                    }

                    txtCity = "Meridian";

                    BingMaps bm = new BingMaps(BingLicenseKey);

                    string RetStr = bm.GeocodeAddressString(AddressLine1 + " Meridian," + "ID");

                    if (RetStr != "No Results Found")
                    {
                        string[] RetVals = RetStr.Split(',');
                        //lblLat = RetVals[0].ToString();
                        //lblLong = RetVals[1].ToString();
                        //txtZipCode = RetVals[2].ToString();
                        //   txtCity = RetVals[3].ToString();


                        Address = RetVals[4].ToString();

                        AddressSplitter = Address.Split(' ');

                        if (AddressSplitter.Length == 3)
                        {
                            txtStreetNo = AddressSplitter[0].ToString();
                            cboAddressDirection = AddressSplitter[1].ToString();

                            if (cboAddressDirection.Length > 2)
                            {
                                cboAddressDirection = "";
                                txtStreetNameAddress = AddressSplitter[1].ToString();
                                cboAddressSuffix = AddressSplitter[2].ToString();


                            }
                            else
                            {

                                txtStreetNameAddress = AddressSplitter[2].ToString();
                                cboAddressSuffix = AddressSplitter[3].ToString();
                            }



                        }
                        else if (Address.Length == 4)
                        {
                            txtStreetNo = AddressSplitter[0].ToString();
                            cboAddressDirection = AddressSplitter[1].ToString();
                            txtStreetNameAddress = AddressSplitter[2].ToString();
                            try
                            {
                                cboAddressSuffix = AddressSplitter[3].ToString();
                            }
                            catch { }
                        }


                        txtCity = RetVals[3].ToString();

                        // GetRDFromLatLong(lblLat.Text, lblLong.Text);
                    }

                }
                else
                {




                    BingMaps bm = new BingMaps(BingLicenseKey);

                    string RetStr = bm.GeocodeAddressString(AddressLine1 + " Meridian," + "ID");

                    if (RetStr != "No Results Found")
                    {
                        string[] RetVals = RetStr.Split(',');
                        //lblLat = RetVals[0].ToString();
                        //lblLong = RetVals[1].ToString();
                        //txtZipCode = RetVals[2].ToString();
                        //   txtCity = RetVals[3].ToString();


                        Address = RetVals[4].ToString();

                        AddressSplitter = Address.Split(' ');
                        txtStreetNo = AddressSplitter[0].ToString();
                        cboAddressDirection = AddressSplitter[1].ToString();
                        txtStreetNameAddress = AddressSplitter[2].ToString();
                        try
                        {
                            cboAddressSuffix = AddressSplitter[3].ToString();
                        }
                        catch { }


                        txtCity = RetVals[3].ToString();

                        // GetRDFromLatLong(lblLat.Text, lblLong.Text);
                    }

                    //Cursor = Cursors.Default;


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error Parsing Addresss! " + ex.Message.ToString());

            }
        }


        private string GetRDFromAddress(string Address)
        {
            try
            {

                string txtStreetNo;
                string cboAddressDirection;
                string txtStreetNameAddress;
                string cboAddressSuffix;
                string txtCity;
                ExtractAddressPiecesFromAddress(Address, out txtStreetNo, out cboAddressDirection, out txtStreetNameAddress, out cboAddressSuffix, out txtCity);


                // if (

                if (txtStreetNo != "")
                {
                    DataView dv = DataDB.ExecWithResultsDataView("sel_RD_from_address " + txtStreetNo + ",'" + cboAddressDirection + "','" + txtStreetNameAddress.Replace("'", "''").Trim() + "','" + cboAddressSuffix.Replace("'", "''").Trim().Trim() + "','" + txtCity.Trim() + "'");



                    foreach (DataRowView dr in dv)
                    {
                        return dr["rd"].ToString();

                    }
                }

                return "null";



            }
            catch (Exception ex)
            {
                MessageBox.Show("Error getting RD! " + ex.Message.ToString());
                return "null";
            }

        }


        private bool SaveContactIdenitifiableInfo(string ContactID, string DOB, string age, string SSN, string Gender, string Race, string Ethnicity)
        {
            try
            {

                string ErrorMessage = "";


                if (age == "")
                    age = "null";

                if (DOB == "")
                    DOB = "null";
                else
                    DOB = "'" + DOB + "'";


                try
                {
                    if (int.Parse(age) < 0)
                        age = "0";
                }
                catch { }

                string SQL = "ins_contact_identification " + ContactID + "," + DOB + "," + age + ",'" + SSN.Trim().Replace("'", "''") + "','" + "" +
                        "','" + "ID" + "','" + Gender + "','" + Race.Trim().Replace("'", "''") + "'," + "0" + "," + "0" + "," + "0" + "," + "0" +
                        "," + "0" + "," + "0" + "," + "0" + "," + "0" + ",'" + "H&W Referral" + "','" + "" + "','" +
                        "" + "','" + Ethnicity + "'";


                if (DataDB.ExecDBInsert(SQL, ref ErrorMessage) == false)
                {
                    pf.SendMail("HNWDocParser", "hwreferrals", "", "Failed to insert contact identification for H&W Contact ID " + ContactID, true, ErrorMessage);

                    return false;
                }
                return true;

            }
            catch (Exception ex)
            {
                return false;

            }

        }


        private string InsertContactIntoITS(string Name, string DOB, string Gender, string Ethnicity, string Race, string SSN, string ClientID, string Role, string HomePhoneNumber, string BusinessPhoneNumber, string Age, bool IsBusiness, string PINo)
        {

            try
            {
                string ErrorMessage = "";

                string[] Names = Name.Split(',');
                string LastName = Names[0].ToString().Trim();
                string FirstName = "";

                string MiddleName = "UNKNOWN";

                try
                {
                    FirstName = Names[1].ToString().Trim();
                }
                catch
                {
                    MiddleName = "";
                }

                string ContactID = "";



                string[] FirstMiddleNames = FirstName.Split(' ');

                if (FirstMiddleNames.Length > 1)
                {
                    FirstName = FirstMiddleNames[0].ToString();
                    MiddleName = FirstMiddleNames[1].ToString();

                }

                string InvolvementType = "";


                //if (Role.ToUpper().Trim() == "CHILD OF CONCERN")
                //    InvolvementType = "Victim";
                //else if (Role.ToUpper().Trim() == "CARETAKER")
                //    InvolvementType = "Guardian/Parent";
                //else if (Role.ToUpper().Trim() == "REPORTING PARTY")
                //    InvolvementType = Role;
                //else
                //    InvolvementType = "Unknown";

                InvolvementType = Role;

                if (InvolvementType.Trim().Length < 3)
                {
                    InvolvementType = "UNKNOWN";

                }


                ContactID = "null";
                if (int.Parse(ClientID) > 0)
                {

                    DataView dv = DataDB.ExecWithResultsDataView("sel_contact_id_from_client_id " + ClientID);


                    foreach (DataRowView dr in dv)
                    {
                        ContactID = dr["contact_id"].ToString();


                    }
                }


                // Do a update if the InContactId is not null
                string SQL = "ins_contact_2 '" + LastName.Trim().Replace("'", "''") + "','" + FirstName.Trim().Replace("'", "''") + "','" +
                             MiddleName.Trim().Replace("'", "''") + "','" + "" + "'," + "0" + ",'" +
                             HomePhoneNumber.Trim().Replace("'", "''") + "','" + "" + "','" + BusinessPhoneNumber.Trim().Replace("'", "''") + "','" +
                             "" + "','" + "" + "','" +
                             "" + "','" + InvolvementType + "'," + "0" + "," + "null" +
                             "," + ContactID + ",'" + "" + "'," + "0" + "," + "0";


                ContactID = DataDB.ExecWithSingleResult(SQL, ref ErrorMessage);

                if (ErrorMessage.Trim().Length > 2)
                {
                    pf.SendMail("HNWDocParser", "hwreferrals", "", PINo + " failed to insert contact " + Name + " into ITS!", true, ErrorMessage);



                }

                SaveContactIdenitifiableInfo(ContactID, DOB, Age, SSN, Gender, Race, Ethnicity);



                return ContactID;
            }
            catch (Exception ex)
            {

                return "";
            }


        }

        private bool SaveContactAddress(string contactID, string Address, string BusinessName, string BusinessAddresss, string CityStateZip)
        {
            try
            {
                string ErrorMessage = "";
                string City = "";
                string State = "";
                string Zip = "";

                string AddressCity = "";
                string AddressState = "";
                string AddressZip = "";

                string[] Splitter = CityStateZip.Split(',');

                City = Splitter[0].ToString().Trim();

                if (Splitter.Length == 3)
                {
                    State = Splitter[1].ToString().Trim();
                    Zip = Splitter[2].ToString().Trim();

                }
                else
                {

                    string[] Splitter2 = Splitter[1].ToString().Trim().Split(' ');
                    State = Splitter2[0].ToString().Trim();
                    Zip = Splitter2[1].ToString().Trim();
                }


                if (Address.Contains("\r") == true)
                {
                    string[] AddressSplitter = Address.Split('\r');
                    Address = AddressSplitter[0].ToString();

                    string[] Splitter3 = AddressSplitter[AddressSplitter.Length - 1].Split(',');
                    AddressCity = Splitter3[0].ToString().Trim();
                    AddressState = Splitter3[1].ToString().Trim();
                    AddressZip = Splitter3[2].ToString().Trim();

                }




                string SQL = "";


                SQL = "ins_contact_address " + contactID + ",'" + "" + "','" + "" + "','" +
                    Address.Trim().Replace("'", "''") + "','" + "" + "','" + "" + "','" +
                    AddressCity + "','" + AddressState + "','" + AddressZip + "','" +
                    BusinessName.Trim().Replace("'", "''") + "','" + BusinessAddresss.Trim().Replace("'", "''") + "','" + "" + "','" +
                    City + "','" + State + "','" + Zip + "'";



                if (DataDB.ExecDBInsert(SQL, ref ErrorMessage) == false)
                {

                    pf.SendMail("HNWDocParser", "hwreferrals", "", "Failed to insert contact addresss for H&W Contact ID " + contactID, true, ErrorMessage);
                    // MessageBox.Show("Error Saving Contact Address! " + ErrorMessage);
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }


        }


        private string Test(Document doc, int TableCounter, string Role, int RowCounter)
        {
            Role = doc.Tables[TableCounter + 2].Cell(RowCounter, 4).Range.Text.Trim().Replace("\r\a", "");
            return Role;
        }


        private bool ConvertImage(string FileName, string incident_support_doc_id, ref string Attachment_GUID, ref string PageNo, string Title, string FurtherDesc, ref string ConversionError)
        {
            try
            {
                FileInfo fi = new FileInfo(FileName);


                string Extension = fi.Extension.ToLower();
                string AttachmentType = DataDB.ExecWithSingleResult("sel_attachment_type_from_extension '" + fi.Extension + "'");
                string FileCreationDate = fi.LastWriteTime.ToString();
                long FileSizeInBytes;
                string ErrorMessage = ""; ;


                if (fi.Extension == ".pdf" || fi.Extension == ".tif" || fi.Extension == ".tiff")
                    AttachmentType = "SUPPORTING_DOCUMENT";


                string ConvertedFilename = "";
                Image contactPhoto;
                byte[] ContactImageBytes;
                FileStream fs;
                BinaryReader reader;
                Image newImage = null;


                byte[] imgBytes;

                byte[] thumbnailPhoto;

                MemoryStream stream;


                if (fi.Extension.ToUpper() != ".JPG")
                {

                    // Convert Image to JPG

                    /*
                    lstStatus.Items.Insert(0, "Converting " + fi.Name + " to a JPG");
                    Application.DoEvents();


                    //// Open file and Read into Byte Array
                    fs = new FileStream(fi.FullName, FileMode.Open);

                    reader = new BinaryReader(fs);
                    ContactImageBytes = reader.ReadBytes((int)fs.Length);
                    fs.Close();



                    // Create Memory stream from Bytes
                    stream = new MemoryStream(ContactImageBytes);


                    // Create a new Image from the stream 
                    newImage = Image.FromStream(stream);

                    // Create a new File
                    //string ConvertedFilename = System.IO.Path.GetTempFileName().Replace(".tmp", "") + ".jpg";

                    ConvertedFilename = m.TempDir + fi.Name.Replace(fi.Extension, ".jpg");
                    // Save out New Image to File


                    EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, (long)0);
                    System.Drawing.Imaging.EncoderParameters encoderParams = new EncoderParameters();
                    encoderParams.Param[0] = qualityParam;

                    ImageCodecInfo jpegCodec = GetEncoderInfo("image/jpeg");


                      




                    
                    newImage.Save(ConvertedFilename,jpegCodec,  encoderParams);
                    */

                    ConvertedFilename = FileName;

                }
                else
                {
                    ConvertedFilename = FileName;

                }

                // Open File just Created
                fs = new FileStream(FileName, FileMode.Open);

                // open FIle and get bytes that will be stored in db
                reader = new BinaryReader(fs);
                ContactImageBytes = reader.ReadBytes((int)fs.Length);
                fs.Close();


                // Get byte stream
                stream = new MemoryStream(ContactImageBytes);


                // Create a new Image from the stream 
                newImage = Image.FromStream(stream);


                // Open New File and Get it's properties

                //picContactPhoto.Image = newImage;
                contactPhoto = newImage;





                fi = new FileInfo(ConvertedFilename);
                fi.IsReadOnly = false;


                Extension = fi.Extension;
                FileSizeInBytes = fi.Length / 1024;
                FileName = fi.Name.ToLower();


                string[] FileNo = FileName.Split('.');
                PageNo = FileNo[0].ToString().Replace("pdf", "").Replace("new_", "");



                // Set thumnail


                Image ThumbPic = null;


                //string NewFilename = System.IO.Path.GetTempFileName().Replace(".tmp", "") + ".jpg";
                //ThumbPic = CreateThumbmailImage(fi.FullName, NewFilename);

                lb.Items.Add("Uploading  " + FileName.Replace(".pdf", ""));



                if (DataDB.ExecDBInsertAttachmentImage(ContactImageBytes, null, ref Attachment_GUID, AttachmentType, Title.Trim().Replace("'", "''") + " Page# " + PageNo,
                     FileName.Trim().Replace("'", "''"), Extension.Trim().Replace("'", "''"), FileSizeInBytes.ToString(), FileCreationDate,
                     FurtherDesc.Trim().Replace("'", "''"), "0", ref ErrorMessage) == false)
                {
                    ConversionError = ErrorMessage;
                    MessageBox.Show(ErrorMessage);
                    return false;

                }
            }
            catch (Exception ex)
            {
                ConversionError = ex.Message.ToString();
                System.Windows.Forms.MessageBox.Show(ex.ToString());
                return false;
            }
            return true;

        }


        private int CalculateAge(string DOB_in)
        {
            try
            {

                if (DOB_in == "")
                {

                    return -1;
                }
                //long years = Microsoft.VisualBasic.DateAndTime.DateDiff(Microsoft.VisualBasic.DateInterval.Year, DateTime.Parse(dtDateofBirth.Text), DateTime.Now, Microsoft.VisualBasic.FirstDayOfWeek.System, Microsoft.VisualBasic.FirstWeekOfYear.System);

                DateTime dtDOB = DateTime.Parse(DOB_in);

                //System.TimeSpan diffResult = DateTime.Now.Subtract(dtDOB);



                // get the difference in years
                int years = DateTime.Now.Year - dtDOB.Year;
                // subtract another year if we're before the
                // birth day in the current year
                if (DateTime.Now.Month < dtDOB.Month ||
                    (DateTime.Now.Month == dtDOB.Month &&
                    DateTime.Now.Day < dtDOB.Day))
                    years--;


                return years;








            }
            catch
            {
                return -1;
                //MessageBox.Show(ex.Message.ToString()); 

            }

        }


        private bool BustPDFIntoImagesGD(string FileName, ref string OutputFolder, ref string ErrorMessage)
        {
            try
            {
                // MrDo

                //return true;

                //GdPictureStatus Status = GdPictureStatus.OK;


                // GdPicture10.LicenseManager lm = new GdPicture10.LicenseManager();





                int yy = oGdPictureImaging.CreateGdPictureImageFromFile(FileName);

                // oGdPictureImaging.cre

                //oGdPictureImaging.SaveAsTIFF(1, @"f:\out.tif", TiffCompression.TiffCompressionAUTO);

                //pdf.SetLicenseNumber("726322766916098671420143053635028");


                pdf.LoadFromFile(FileName, false);

                int page_count = pdf.GetPageCount();


                // Create Unique Folder Name
                string NewFolderName = System.IO.Path.GetRandomFileName();

                // Make sure Folder Doesn't exist
                while (Directory.Exists(TempDir + NewFolderName) == true)
                {
                    NewFolderName = System.IO.Path.GetRandomFileName();
                }

                Directory.CreateDirectory(TempDir + NewFolderName);

                FileInfo fi = new FileInfo(FileName);

                System.IO.File.Copy(FileName, TempDir + NewFolderName + @"\" + fi.Name);


                OutputFolder = TempDir + NewFolderName;



                for (int x = 1; x <= page_count; x++)
                {
                    string WorkingName = "new_" + pf.Right("00000" + x.ToString(), 5);

                    System.Windows.Forms.Application.DoEvents();




                    pdf.SelectPage(x);

                    // rasterize the active page
                    int RasterizedPageID = pdf.RenderPageToGdPictureImage(300, false);



                    //if (optBlackAndWhite.Checked)
                    //{


                    //oGdPictureImaging.ConvertTo1Bpp(RasterizedPageID);


                    //if (optBlackAndWhite.Checked == true)
                    //  {


                    lb.Items.Add("Creating image " + WorkingName + ".tiff" + " (" + x.ToString() + " of " + page_count.ToString() + ")");
                    lb.TopIndex = lb.Items.Count - 1;
                    System.Windows.Forms.Application.DoEvents();

                    //Application.DoEvents();


                    //oGdPictureImaging.FxBlackNWhite(RasterizedPageID, BitonalReduction.Stucki);
                    //oGdPictureImaging.ConvertTo1BppFast(RasterizedPageID);


                    oGdPictureImaging.ConvertTo4Bpp16(RasterizedPageID);

                    oGdPictureImaging.Resize(RasterizedPageID, 1600, 1200, System.Drawing.Drawing2D.InterpolationMode.Default);
                    Status = oGdPictureImaging.SaveAsTIFF(RasterizedPageID, TempDir + NewFolderName + "\\" + WorkingName + ".tiff", TiffCompression.TiffCompressionAUTO);
                    oGdPictureImaging.ReleaseGdPictureImage(RasterizedPageID);



                    //  }
                    //else
                    //{
                    //    lstStatus.Items.Insert(0, "Creating image " + WorkingName + ".jpg" + " (" + x.ToString() + " of " + page_count.ToString() + ")");
                    //    Application.DoEvents();

                    //    oGdPictureImaging.Resize(RasterizedPageID, 1600, 1200, System.Drawing.Drawing2D.InterpolationMode.Default);
                    //    Status = oGdPictureImaging.SaveAsJPEG(RasterizedPageID, m.TempDir + NewFolderName + "\\" + WorkingName + ".jpg", 60);
                    //    oGdPictureImaging.ReleaseGdPictureImage(RasterizedPageID);


                    //}


                    //}
                    //else
                    //{


                    //}

                }



                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(ex.ToString());
                return false;
            }


        }


        public void GetFamilyProfile(Document doc, int TableNo, string Name, out string Gender, out string Relationship, out string DOB, out string ethnicity, out string Race)
        {

            Relationship = "";
            DOB = "";
            ethnicity = "";
            Race = "";
            Gender = "";


            try
            {




                int RowCounter = 1;
                foreach (Row r in doc.Tables[TableNo].Rows)
                {
                    if (RowCounter > 1)
                    {

                        if (doc.Tables[TableNo].Cell(RowCounter, 0).Range.Text.Contains(Name))
                        {

                            string Value = doc.Tables[TableNo].Cell(RowCounter, 0).Range.Text.Trim();

                            string[] Splitter = Value.Split(':');
                            Relationship = Splitter[1].ToString().Replace("\r\a", "").Trim();



                            Gender = doc.Tables[TableNo].Cell(RowCounter, 2).Range.Text.Trim().Replace("\r\a", "").Trim();
                            DOB = doc.Tables[TableNo].Cell(RowCounter, 3).Range.Text.Trim().Replace("\r\a", "").Trim();
                            ethnicity = doc.Tables[TableNo].Cell(RowCounter, 4).Range.Text.Trim().Replace("\r\a", "").Trim();
                            Race = doc.Tables[TableNo].Cell(RowCounter, 6).Range.Text.Trim().Replace("\r\a", "").Trim();
                            break;
                        }
                    }

                    RowCounter++;
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Error parsing Family Profile! " + ex.Message.ToString());


            }



        }


        public string Mid(string s, int a, int b)
        {
            string temp = s.Substring(a - 1, b);
            return temp;
        }


    }
}

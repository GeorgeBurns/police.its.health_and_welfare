﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;



using HealthAndWelfareDocParser.SearchService;
using HealthAndWelfareDocParser.GeocodeService;





using System.Web;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;





namespace HealthAndWelfareDocParser
{
    class BingMaps
    {
        string key = "";


        public BingMaps(string LicenseKey)
        {
            this.key = LicenseKey;


        }


        public String GeocodeAddressString(string address)
        {
            string results = "";
            GeocodeResponse geocodeResponse = GeocodeAddressGeocodeResponse(address);

            if (geocodeResponse.Results.Length > 0)
            {
                results = String.Format("{0},{1},{2},{3},{4}", geocodeResponse.Results[0].Locations[0].Latitude, geocodeResponse.Results[0].Locations[0].Longitude, geocodeResponse.Results[0].Address.PostalCode, geocodeResponse.Results[0].Address.Locality, geocodeResponse.Results[0].Address.AddressLine);

                Console.WriteLine(geocodeResponse.Results[0].Address.PostalCode);
            }
            else
                results = "No Results Found";

            return results;
        }

        private GeocodeResponse GeocodeAddressGeocodeResponse(string address)
        {
            GeocodeRequest geocodeRequest = new GeocodeRequest();

            // Set the credentials using a valid Bing Maps key
            geocodeRequest.Credentials = new HealthAndWelfareDocParser.GeocodeService.Credentials();
            geocodeRequest.Credentials.ApplicationId = key;

            // Set the full address query
            geocodeRequest.Query = address;

            // Set the options to only return high confidence results 
            ConfidenceFilter[] filters = new ConfidenceFilter[1];
            filters[0] = new ConfidenceFilter();
            filters[0].MinimumConfidence = HealthAndWelfareDocParser.GeocodeService.Confidence.High;

            // Add the filters to the options
            GeocodeOptions geocodeOptions = new GeocodeOptions();
            geocodeOptions.Filters = filters;
            geocodeRequest.Options = geocodeOptions;

            // Make the geocode request
            GeocodeServiceClient geocodeService = new GeocodeServiceClient("BasicHttpBinding_IGeocodeService");
            GeocodeResponse geocodeResponse = geocodeService.Geocode(geocodeRequest);
            return geocodeResponse;
        }


        private void GetCurrentLocation()
        {

            try
            {

               
                


            }
            catch (Exception ex)
            {

            }


        }




       


    }
}

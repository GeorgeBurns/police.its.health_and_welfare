﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading.Tasks;
using Microsoft.Exchange.WebServices.Data;
using System.Net.Mail;
using System.Net;
using Microsoft.Win32;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace HealthAndWelfareMailDataImporter
{
    public partial class frmMain : Form
    {
        string FileName = "";

        bool ReadyForAttachment = false;

        public frmMain()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            CreateBrowserKey();
            tmrStart.Enabled = true;
        }




        private void timer1_Tick(object sender, EventArgs e)
        {

            Application.DoEvents();
            StartExchangeService();
            tmrStart.Enabled = false;



        }

        private void StartExchangeService()
        {
            try
            {

                //  WebBrowser wb = new WebBrowser();
                // wb.Navigate(@"C:\securedoc_20150903T131422.html");


                while (1 == 1)
                {


                    ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2010_SP2);

                    service.Credentials = new NetworkCredential("hwautomation", "Bl@ckH@wk$", "meridian");
                    service.PreAuthenticate = true;
                    service.AutodiscoverUrl("hwautomation@meridiancity.org");

                    FindItemsResults<Item> findResults = service.FindItems(WellKnownFolderName.Inbox, new ItemView(25));


                    if (findResults.Items.Count == 0)
                        lstResults.Items.Clear();

                    lstResults.Items.Insert (0, "Found " + findResults.Items.Count.ToString() + " messages");
                    //Application.DoEvents();

                    foreach (Item item in findResults.Items)
                    {
                        //Use the name resolution to extract the SMTP Address - Parse UserName From SMTP Prefix
                        var resolvedNames = service.ResolveName(item.LastModifiedName);
                        // string thisSMTP = resolvedNames[0].Mailbox.Address;
                        // string[] splitSMTP = thisSMTP.Split('@');
                        //string thisUserName = splitSMTP[0].ToString();

                        //Get The Body oF The Message
                        item.Body = new MessageBody(BodyType.Text, "Body");
                        PropertySet myPropertySet = new PropertySet(BasePropertySet.FirstClassProperties);
                        myPropertySet.RequestedBodyType = BodyType.Text;
                        EmailMessage message = EmailMessage.Bind(service, item.Id, myPropertySet);

                        if (message.HasAttachments == true)
                        {
                            Microsoft.Exchange.WebServices.Data.FileAttachment a = message.Attachments[0] as FileAttachment;


                            lstResults.Items.Insert(0, "Saving " + a.Name);
                          //  Application.DoEvents();

                            a.Load(a.Name);

                            string FullPath = Application.StartupPath;


                            lstResults.Items.Insert(0, "Navigating.. " + a.Name);
                            //Application.DoEvents();

                            webBrowser1.Navigate(FullPath + @"\" + a.Name);

                            //string FullPath = Application.StartupPath;

                            //System.Diagnostics.Process proc = new System.Diagnostics.Process();
                            //proc.EnableRaisingEvents = false;
                            //proc.StartInfo.FileName = "PageOpener.exe";
                            //proc.StartInfo.Arguments = FullPath + @"\" + a.Name;
                            //// proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                            //proc.StartInfo.UseShellExecute = true;
                            //proc.StartInfo.WorkingDirectory = FullPath;

                            //proc.Start();
                            //proc.WaitForExit();



                        }

                        item.Delete(DeleteMode.MoveToDeletedItems);




                    }

                    System.Threading.Thread.Sleep(30000);
                }


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());

            }


        }

        string URL = "";

        static string BrowserKeyPath = @"\SOFTWARE\Microsoft\Internet Explorer\MAIN\FeatureControl\FEATURE_BROWSER_EMULATION";
        //32bit OS
        //\Software\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BROWSER_EMULATION
        //32bit app on 64bit OS
        //\SOFTWARE\Wow6432Node\Microsoft\Internet Explorer\MAIN\FeatureControl\FEATURE_BROWSER_EMULATION
        public static void RemoveBrowserKey()
        {
            RegistryKey key = Microsoft.Win32.Registry.CurrentUser.OpenSubKey(BrowserKeyPath.Substring(1), true);
            key.DeleteValue(Process.GetCurrentProcess().ProcessName + ".exe", false);

        }
        public static void CreateBrowserKey(bool IgnoreIDocDirective = false)
        {
            string basekey = Microsoft.Win32.Registry.CurrentUser.ToString();
            Int32 value = 11000;
            //Value reference: http://msdn.microsoft.com/en-us/library/ee330730%28v=VS.85%29.aspx
            //IDOC Reference:  http://msdn.microsoft.com/en-us/library/ms535242%28v=vs.85%29.aspx
            WebBrowser browser = new WebBrowser();

            switch (browser.Version.Major)
            {
                case 8:
                    if (IgnoreIDocDirective)
                    {
                        value = 8888;
                    }
                    else
                    {
                        value = 8000;
                    }
                    break;
                case 9:
                    if (IgnoreIDocDirective)
                    {
                        value = 9999;
                    }
                    else
                    {
                        value = 9000;
                    }
                    break;
                case 10:
                    if (IgnoreIDocDirective)
                    {
                        value = 10001;
                    }
                    else
                    {
                        value = 10000;
                    }
                    break;
                case 11:
                    if (IgnoreIDocDirective)
                    {
                        value = 11001;
                    }
                    else
                    {
                        value = 11000;
                    }
                    break;
                default:
                    break;
            }

            Microsoft.Win32.Registry.SetValue(Microsoft.Win32.Registry.CurrentUser.ToString() + BrowserKeyPath, Process.GetCurrentProcess().ProcessName + ".exe", value, Microsoft.Win32.RegistryValueKind.DWord);
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

           

            HtmlDocument doc = this.webBrowser1.Document;

            //doc.GetElementById("passwordInput1.text").SetAttribute("Value", "1234");

            //  HtmlElement ee = doc.GetElementById("passwordInput1.text");
            // return;

            if (ReadyForAttachment == true)
            {

                HtmlElementCollection links = webBrowser1.Document.GetElementsByTagName("a");


                foreach (HtmlElement link in links)
                {
                    try
                    {
                        //if (link.InnerText.Equals("My Assigned"))
                        //  link.InvokeMember("Click");

                        // Console.WriteLine(link.InnerText);

                        System.Diagnostics.Debug.WriteLine(link.InnerText);

                        string href = link.GetAttribute("href");
                        string Filename = link.InnerText;
                        if (Filename != null)
                        {
                            if (Filename.Contains(".doc") == true)
                            {
                                //webBrowser1.Navigate(href);

                                timer1.Enabled = false;

                                DownloadFileThread df = new DownloadFileThread();

                                lstResults.Items.Insert(0, "Downloading.. " + Filename);
                                Application.DoEvents();

                                df.DownloadFile(href, Filename);

                                Console.WriteLine("Sleeping for 2 seconds");
                                System.Threading.Thread.Sleep(2000);

                                string FullPath = Application.StartupPath;


                                lstResults.Items.Insert(0, "Opening.. " + Filename);

                                System.Diagnostics.Process proc = new System.Diagnostics.Process();
                                proc.EnableRaisingEvents = false;
                                proc.StartInfo.FileName = "HNWDocParser.exe";
                                proc.StartInfo.Arguments = FullPath + @"\" + Filename;
                                // proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
                                proc.StartInfo.UseShellExecute = true;
                                proc.StartInfo.WorkingDirectory = FullPath;

                                proc.Start();
                                proc.WaitForExit();


                                this.Close();


                                ///WebClient webClient = new WebClient();

                                // Creates a webclient
                                //webClient.DownloadFileCompleted += new AsyncCompletedEventHandler(Completed);                   // Uses the Event Handler to check whether the download is complete
                                //webClient.DownloadProgressChanged += new DownloadProgressChangedEventHandler(ProgressChanged);  // Uses the Event Handler to check for progress made
                                //webClient.DownloadFileAsync(new Uri(href), @"C:\" + Filename);           // Defines the URL and destination directory for the downloaded file

                            }
                        }




                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error downloading! " + ex.Message.ToString());
                    }

                }

            }


            // passwordEntry1

            System.Threading.Thread.Sleep(2000);

            webBrowser1.Focus();
            SendKeys.Send("{ENTER}");

            timer1.Enabled = true;
            
            ReadyForAttachment = true;

            return;

 
        }

        private void webBrowser1_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
            WebClient client = new WebClient();

            client.DownloadDataCompleted += new DownloadDataCompletedEventHandler(client_DownloadDataCompleted);

            client.DownloadDataAsync(e.Url);
        }

        void client_DownloadDataCompleted(object sender, DownloadDataCompletedEventArgs e)
        {
            //string filepath = textBox1.Text;
            //File.WriteAllBytes(filepath, e.Result);
            // MessageBox.Show("File downloaded");
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            RemoveBrowserKey();
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            webBrowser1.Focus();
            SendKeys.Send("{ENTER}");
        }


    }
}
